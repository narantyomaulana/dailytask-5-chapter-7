import '../App.css';
import ListLanguage from '../components/LanguageComponent';
import Header from '../components/Header';
import {useParams} from 'react-router-dom';


function Javascript() {
    let {id} = useParams();
    const languageList = [
      {
        name: 'HTML & CSS',
        image: 'https://s3-ap-northeast-1.amazonaws.com/progate/shared/images/lesson/react/html.svg'
      },
      {
        name: 'JavaScript',
        image: 'https://s3-ap-northeast-1.amazonaws.com/progate/shared/images/lesson/react/es6.svg'
      },
      {
        name: 'React',
        image: 'https://s3-ap-northeast-1.amazonaws.com/progate/shared/images/lesson/react/react.svg'
      },
      {
        name: 'Ruby',
        image: 'https://s3-ap-northeast-1.amazonaws.com/progate/shared/images/lesson/react/ruby.svg'
      },
      {
        name: 'Ruby on Rails',
        image: 'https://s3-ap-northeast-1.amazonaws.com/progate/shared/images/lesson/react/rails.svg'
      },
      {
        name: 'Python',
        image: 'https://s3-ap-northeast-1.amazonaws.com/progate/shared/images/lesson/react/python.svg'
      }
    ];
  
    return (
      <div className="App">
        <h1>ID :{id}</h1>
        <Header />
        {languageList.map((languageItem) => {
          if(languageItem.name == id ){
            return (
              <ListLanguage
                name={languageItem.name}
                image={languageItem.image}
          />
            )
          }
        })}    
      </div>
    );
  }
  
  export default Javascript;
  